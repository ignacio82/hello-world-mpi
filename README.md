#This is a simple MPI hello world Fortran program
##To get this code and run it just do the following:
```BASH
mkdir bitbucket
cd bitbucket
git clone git@bitbucket.org/ignacio82/hello-world-mpi.git
cd mpi
make
mpiexec -n 2 mpi_test.X 
```
